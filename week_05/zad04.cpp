#include "Graph.hpp"
#include "Simple_window.hpp"

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window
	Simple_window win(tl, 800, 1000, "My window");    // make a simple window
	
	Vector_ref<Graph_lib::Rectangle> rects;
	int Top_leftx = 0;
	int Top_lefty = 0;
	int width = 100;
	int height = 100;
	int grid_width = 8;
	int grid_height = 8;
	//Create assisting fields
	for (int i = 0; i < grid_height; ++i)
	{
		for (int j = 0; j < grid_width; ++j)
		{
			rects.push_back(new Graph_lib::Rectangle{ Point {Top_leftx, Top_lefty}, Point{Top_leftx + width, Top_lefty + height } });
			Top_leftx += width;
		}
		Top_lefty += height;
		Top_leftx -= (grid_width * width); // reset Top_leftx to initial value;
	}
	//fill colours
	for (int i = 0; i < rects.size(); ++i)
	{
		if (i * grid_width >= rects.size()) {
			break;
		}
		rects[i*grid_width + i].set_fill_color(Color::red);
	}
	//Print fields
	for (int i = 0; i < rects.size(); ++i)
	{
		win.attach(rects[i]);
	}

	Image skrzat1{ Point{ 200, 0 }, "skrzat.jpg" };
	Image skrzat2{ Point{ 400, 0 }, "skrzat.jpg" };
	Image skrzat3{ Point{ 600, 200 }, "skrzat.jpg" };
	win.attach(skrzat1);
	win.attach(skrzat2);
	win.attach(skrzat3);

	Image drake{ Point{400, 200}, "dragon.jpg" };
	win.attach(drake);
	int rounds = 10;
	int xdirection = 0, ydirection = 0; //step initialization
	for (int i = 0; i < rounds; ++i)
	{
		win.wait_for_button(); //first step goes to right
		switch (i % 4)
		{
		case 0: //first step to the right
			xdirection = 1;
			ydirection = 0;
			break;
		case 1: //second step down
			xdirection = 0;
			ydirection = 1;
			break;
		case 2: //third step to the left
			xdirection = -1;
			ydirection = 0;
			break;
		case 3: //fourth step up
			xdirection = 0;
			ydirection = -1;
			break;
		default:
			xdirection = 0;
			ydirection = 0;
		}
		drake.move(100 * xdirection, 100 * ydirection);
		
	}




	win.wait_for_button();       // give control to the display engine
}