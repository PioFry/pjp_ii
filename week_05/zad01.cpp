#include "Graph.hpp"
#include "Simple_window.hpp"

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window

	Simple_window win(tl, 600, 400, "My window");    // make a simple window
	Circle c{ Point{ 100,200 },50 };
	Graph_lib::Ellipse e{ Point{ 100,200 }, 75,25 };
	e.set_color(Color::dark_red);
	Mark m{ Point{ 100,200 },'x' };
	ostringstream oss;
	oss << "screen size: " << x_max() << "*" << y_max()
		<< "; window size: " << win.x_max() << "*" << win.y_max();
	Text sizes{ Point{ 100,20 },oss.str() };
	sizes.set_font(Font::helvetica);
	c.set_color(Color::dark_green);
	
	win.attach(sizes);
	win.attach(e);
	win.attach(c);

	win.wait_for_button();       // give control to the display engine
}