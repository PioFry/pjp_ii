#include "Graph.hpp"
#include "Simple_window.hpp"

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window

	Simple_window win(tl, 600, 400, "My window");    // make a simple window
	
	Graph_lib::Rectangle my_rect (Point{ 100, 150 }, Point{ 200, 200 });
	Graph_lib::Polygon my_poly;
	my_poly.add({ 300, 150 });
	my_poly.add({ 400, 150 });
	my_poly.add({ 400, 200 });
	my_poly.add({ 300, 200 });
	my_poly.set_color(Color::red);
	my_rect.set_color(Color::blue);
	Graph_lib::Rectangle txt_rect(Point{ 10, 20 }, Point{ 110, 50 });
	Text txt{ Point {35, 40}, "Howdy!" };
	Text in_1{ Point {200, 200}, "P" };
	Text in_2{ Point{ 400, 200 }, "F" };
	in_1.set_font_size(160);
	in_1.set_font(Font::courier_bold);
	in_1.set_color(Color::dark_cyan);
	in_2.set_font_size(160);
	in_2.set_font(Font::courier_bold);
	in_2.set_color(Color::dark_green);

	Vector_ref<Graph_lib::Rectangle> rects;
	//Point Tic_tac{ 400, 300 };
	int Top_leftx = 400;
	int Top_lefty = 300;
	int width = 30;
	int height = 30;
	int tictac_width = 3;
	int tictac_height = 3;
	//Create assisting fields
	for (int i = 0; i < tictac_height; ++i)
	{
		for (int j = 0; j < tictac_width; ++j)
		{
			rects.push_back(new Graph_lib::Rectangle{ Point {Top_leftx, Top_lefty}, Point{Top_leftx + width, Top_lefty + height } });
			Top_leftx += width;
		}
		Top_lefty += height;
		Top_leftx -= (tictac_width * width); // reset Top_leftx to initial value;
	}
	//fill colours
	for (int i = 0; i < rects.size(); ++i)
	{
		if (i % 2 == 0){
			rects[i].set_fill_color(Color::white);
		}
		else {
			rects[i].set_fill_color(Color::red);
		}
	}
	//Print fields
	for (int i = 0; i < rects.size(); ++i)
	{
		win.attach(rects[i]);
	}
	//smthing from myself:
	Graph_lib::Closed_polyline star;
	star.add({ 500, 200 });
	star.add({ 525, 125 });
	star.add({ 550, 200 });
	star.add({ 500, 150 });
	star.add({ 550, 150 });

	win.attach(star);
	win.attach(in_1);
	win.attach(in_2);
	win.attach(txt);
	win.attach(txt_rect);
	win.attach(my_poly);
	win.attach(my_rect);
	win.wait_for_button();       // give control to the display engine
}