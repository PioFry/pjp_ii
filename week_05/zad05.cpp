#include "Graph.hpp"
#include "Simple_window.hpp"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*		Arc		*/
class My_arc : public Graph_lib::Shape 
{
public:
	My_arc(Point p, double r, double begin, double end, double curve_height);
	int radius() const { return r; };
	void draw_lines() const;
private:
	double r;
	double a_start;
	double a_end;
	double curve;
};

My_arc::My_arc(Point p, double r, double begin, double end, double curve_height)
	:r(r), a_start(begin), a_end(end), curve(curve_height)
{
	add(Point(p.x - r, p.y - r));       // store top-left corner

}

void My_arc::draw_lines() const
{
	if (color().visibility())
		fl_arc(point(0).x, point(0).y, r + r, r + (r - curve), a_start, a_end);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*		box		*/
class My_box : public Graph_lib::Shape
{
public: 
	My_box(Point p, int height, int width, int r);
	void draw_lines() const;
private:
	int h;
	int w;
	Graph_lib::Lines sides;
	Graph_lib::Vector_ref<My_arc> corners;
};

My_box::My_box(Point p, int height, int width, int r)
	:h(height), w(width)
{
	add(Point(p.x - w, p.y - h));       // store top-left corner
	sides.add({ point(0).x + r, point(0).y }, { point(0).x + (w - r), point(0).y });	//top
	sides.add({ point(0).x, point(0).y + r }, { point(0).x, point(0).y + (h - r) });	//left
	sides.add({ point(0).x + w, point(0).y + (h - r) }, { point(0).x + w, point(0).y + r }); // right
	sides.add({ point(0).x + (w - r), point(0).y + h }, { point(0).x + r, point(0).y + h }); //down

	corners.push_back(new My_arc({ p.x - w + r, p.y - h + r }, r, 90, 180, 0)); //top left
	corners.push_back(new My_arc({ p.x - r, p.y - h + r }, r, 0, 90, 0));	//top right
	corners.push_back(new My_arc({ p.x - w + r, p.y - r }, r, 180, 270, 0)); //bottom left
	corners.push_back(new My_arc({ p.x - r, p.y - r }, r, 270, 360, 0));		//bottom right
	
}

void My_box::draw_lines() const
{
	sides.draw_lines();

	for (size_t i = 0; i < corners.size(); ++i)
	{
		corners[i].draw_lines();
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*		Arrow		*/

class My_arrow : public Graph_lib::Shape
{
public:
	My_arrow(Point begin, int lenght, std::string direction); 
	void draw_lines() const;
private:
	Graph_lib::Lines arrow;
};

My_arrow::My_arrow(Point begin, int lenght, std::string direction)
{
	const int tip = lenght * 0.15;
	Point up = { begin.x, begin.y - lenght };
	Point down = { begin.x, begin.y + lenght };
	Point left = { begin.x - lenght, begin.y };
	Point right = { begin.x + lenght, begin.y };
	if (direction == "up")
	{
		arrow.add(begin, up);
		arrow.add(up, { up.x - tip, up.y + tip });
		arrow.add(up, { up.x + tip, up.y + tip });
	}
	else if (direction == "down")
	{
		arrow.add(begin, down);
		arrow.add(down, { down.x - tip, down.y - tip });
		arrow.add(down, { down.x + tip, down.y - tip });
	}
	else if (direction == "left")
	{
		arrow.add(begin, left);
		arrow.add(left, { left.x + tip, left.y + tip });
		arrow.add(left, { left.x + tip, left.y - tip });
	}
	else if (direction == "right")
	{
		arrow.add(begin, right);
		arrow.add(right, { right.x - tip, right.y + tip });
		arrow.add(right, { right.x - tip, right.y - tip });
	}
}

void My_arrow::draw_lines() const
{
	arrow.draw();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*		triangle		*/
class My_triangle : public Graph_lib::Shape
{
public:
	My_triangle(Point tip, int a, int b);
	void draw_lines() const;
private:
	Graph_lib::Lines sides;


};

My_triangle::My_triangle(Point tip, int a, int b)
{
	sides.add(tip, { tip.x + a, tip.y });
	sides.add(tip, { tip.x, tip.y + b });
	sides.add({ tip.x + a, tip.y }, { tip.x, tip.y + b });
}

void My_triangle::draw_lines() const
{
	sides.draw();
}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window
	Simple_window win(tl, 800, 600, "My window");    // make a simple window
	
	My_arc a1({ 100, 100 }, 50, 0, 180, 30);
	win.attach(a1);

	My_box b1({ 300, 300 }, 100, 200, 10);
	win.attach(b1);

	My_arrow ar1({ 400, 200 }, 100, "up");
	win.attach(ar1);

	Point oct_center{ 500, 400 };
	My_triangle tr1(oct_center, 100, 100);
	My_triangle tr2(oct_center, -100, 100);
	My_triangle tr3(oct_center, 100, -100);
	My_triangle tr4(oct_center, -100, -100);
	/*My_triangle tr5(oct_center, 100, 100);
	My_triangle tr6(oct_center, 100, 100);
	My_triangle tr7(oct_center, 100, 100);
	My_triangle tr8(oct_center, 100, 100);*/

	
	win.attach(tr1);
	win.attach(tr2);
	win.attach(tr3);
	win.attach(tr4);


	win.wait_for_button();       // give control to the display engine
}