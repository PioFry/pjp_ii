#include "std_lib_facilities.hpp"


int* ga = new int[10];

void f(int* arr, int size)
{
	int* la = new int[10];
	for (int i = 0; i < 10; i++)
	{
		la[i] = ga[i];
	}
	std::cout << "la array: \n";
	for (int i = 0; i < 10; i++)
	{
		std::cout << i << ": " << la[i] << "\n";
	}
	int* p = new int[size];
	for (int i = 0; i < size; i++)
	{
		p[i] = arr[i];
	}
	std::cout << "p array: \n";
	for (int i = 0; i < size; i++)
	{
		std::cout << i << ": " << p[i] << "\n";
	}
	delete p;
}

int main()
{
	try
	{
		*ga = 1;
		for (int i = 1; i < 10; i++)
		{
			ga[i] = i * 2;
		}

		f(ga, 10);
		int* aa = new int[10];
		int temp = 1;
		*aa = 1;
		for (int i = 1; i < 10; i++)
		{
			aa[i] = (i * temp);
			temp *= i;
		}
		f(aa, 10);

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}
/*
vector<int> vv;
int temp = 1;
for (int i = 1; i <= 10; i++)
{
vv.push_back(i * temp);
temp *= i;
}
std::cout << "Vector vv components: \n";
for (int j = 1; j <= 10; j++)
{
std::cout << vv[j] << "\n";
}
*/