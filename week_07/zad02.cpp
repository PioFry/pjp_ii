#include "std_lib_facilities.hpp"


class Link;

struct God
{
	string name;
	string mythology;
	string vehicle;
	string weapon;
};

class Link {
public:
	struct God god;
	Link(God g, Link* p = nullptr, Link* s = nullptr)
		: god{ g }, prev{ p }, succ{ s } { }
	Link* insert(Link* n); // insert n before this object
	Link* Link::add(Link* n); // insert n after this object
	Link* Link::erase(Link* p); // remove this object from list
	Link* Link::find(Link* p, const string& s); // find s in list
	//const Link* find(const string& s) const; // find s in const list (see �18.5.1)
	Link* Link::advance(Link* p, int n); // move n positions in list
	Link* next() const { return succ; }
	Link* previous() const { return prev; }
	Link* add_ordered(Link* n);
private:
	Link* prev;
	Link* succ;
};

Link* Link::add(Link* n) // add n after this object;
{
	if (n == nullptr) return this;
	if (this == nullptr) return n;
	n->prev = this; // this object comes before n;
	if (succ) succ->prev = n;
	n->succ = succ; // this object'ssuccesor becones n's succesor
	succ = n; // n becomes this objects successor
	return n;
}

Link* Link::insert(Link* n) // insert n before this object; return n
{
	if (n == nullptr) return this;
	if (this == nullptr) return n;
	n->succ = this; // this object comes after n
	if (prev) prev->succ = n;
	n->prev = prev; // this object�s predecessor becomes n�s predecessor
	prev = n; // n becomes this object�s predecessor
	return n;
}

Link* Link::erase(Link* p) // remove *p from list; return p�s successor
{
	if (p == nullptr) return nullptr;
	if (p->succ) p->succ->prev = p->prev;
	if (p->prev) p->prev->succ = p->succ;
	return p->succ;
}

Link* Link::find(Link* p, const string& s) // find s in list;
									 // return nullptr for �not found�
{
	while (p->previous() != nullptr)
	{
		p = p->previous();
	}
	while (p) {
		if (p->god.name == s) return p;
		p = p->succ;
	}
	return nullptr;
}
Link* Link::advance(Link* p, int n) // move n positions in list
							  // return nullptr for �not found�
							  // positive n moves forward, negative backward
{
	if (p == nullptr) return nullptr;
	if (0<n) {
		while (n--) {
			if (p->succ == nullptr) return nullptr;
			p = p->succ;
		}
	}
	else if (n<0) {
		while (n++) {
			if (p->prev == nullptr) return nullptr;
			p = p->prev;
		}
	}
	return p;
}

void print_all(Link* godlist)
{
	while (godlist->previous() != nullptr)
	{
		godlist = godlist->previous();
	}
	while (godlist != nullptr)
	{
		std::cout << "God name: " << godlist->god.name << "\n";
		std::cout << "Mythology: " << godlist->god.mythology << "\n";
		std::cout << "Vehicle: " << godlist->god.vehicle << "\n";
		std::cout << "Weapon: " << godlist->god.weapon << "\n\n";
		godlist = godlist->next();
	}
}

Link* Link::add_ordered(Link* linkedgod)	//adds a god in alphabetical order by name
{
	Link* helper = this;
	while (helper->previous() != nullptr)
	{
		helper = helper->previous();
	}
	while (helper->next() != nullptr)
	{
		if (linkedgod->god.name < helper->god.name)
		{
			helper->insert(linkedgod);
			break;
		}
		else
		{
			helper = helper->next();
		}
	}
	if (helper->next() == nullptr)
	{
		helper->add(linkedgod);
	}
	return linkedgod;
}

int main()
{
	try
	{
		God odin{ "Odin", "Norse", "Eight-legged flying horse called Sleipner","Spear called Gungnir" };
		God zeus{ "Zeus", "Greek", "", "lightning" };
		God thor{ "Thor", "Norse", "", "Hammer calle Mjolnir" };
		God freya{ "Freya", "Norse", "Pegasus", "Some random sword" };
		God loki{ "Loki", "Norse", "", "Daggers" };
		God anubis{ "Anubis", "Egyptian", "", "Staff" };
		God ra{ "Ra", "Egyptian", "Chariot", "bow and arrows" };
		God heracles{ "Herkules", "Greek", "Pegasus", "Mace" };
		Link* norse_gods = new Link{ odin };
		norse_gods->insert(new Link{ freya });
		norse_gods->add(new Link{ thor });
		norse_gods->add_ordered(new Link{ zeus });
		norse_gods->add_ordered(new Link{ loki });
		norse_gods->add_ordered(new Link{ anubis });
		print_all(norse_gods);

		Link* egyptian_gods = new Link{ ra };
		Link* greek_gods = new Link{ heracles };
		Link* to_delete = norse_gods->find(norse_gods, "Zeus");
		norse_gods->erase(to_delete);
		to_delete = norse_gods->find(norse_gods, "Anubis");
		norse_gods->erase(to_delete);
		egyptian_gods->add_ordered(new Link{ anubis });
		greek_gods->add_ordered(new Link{ zeus });
		std::cout << "New golist: \n";
		print_all(norse_gods);
		std::cout << "New golist: \n";
		print_all(greek_gods);
		std::cout << "New golist: \n";
		print_all(egyptian_gods);

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}