#include "std_lib_facilities.hpp"


vector<int> gv = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 };

void f(vector<int> vec)
{
	vector<int> lv(vec.size());
	lv = gv;
	std::cout << "lv vec: \n";
	for (int x : lv)
	{
		std::cout << x << "\n";
	}
	vector<int> lv2 = vec;
	std::cout << "lv2 vec: \n";
	for (int x : lv2)
	{
		std::cout << x << "\n";
	}
}

int main()
{
	try
	{
		f(gv);
		vector<int> vv;
		int temp = 1;
		for (int i = 1; i <= 10; i++)
		{
			vv.push_back(i * temp);
			temp *= i;
		}
		f(vv);
		
		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}
