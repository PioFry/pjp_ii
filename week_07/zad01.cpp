#include "std_lib_facilities.hpp"


void print_array10(ostream& os, int* a)
{
	for (int i = 0; i < 10; i++)
	{
		os << *(a+i);
	}
}

void print_array(ostream& os, int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		os << *(a + i);
	}
}

void print_vector(const vector<int>& vec)
{
	for (size_t i = 0; i < vec.size(); i++)
	{
		std::cout << vec[i];
	}
}

int main()
{
	try
	{
		int const size = 20;
		int elements = 20;
		int arr[size] = { 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120 };
		print_array(std::cout, arr, size);
		vector<int> my_vec;
		for (int i = 0; i <= elements; ++i)
		{
			my_vec.push_back((100+i));
		}
		print_vector(my_vec);
		
		int* p1 = new int{ 7 };
		std::cout << "\n 2nd part \n";
		std::cout << p1 << "\n";
		std::cout << *p1 << "\n";
		int const ndsiz = 7;
		int* p2 = new int[ndsiz];
		for (int i = 0; i < ndsiz; i++)
		{
			p2[i] = 1 + i;
		}
		std::cout << p2 << "\n";
		print_array(std::cout, p2, ndsiz);
		int* p3 = p2;
		p2 = p1;
		p2 = p3;
		std::cout << "\n" << "p1: " << p1 << "\n";
		print_array(std::cout, p1, 1);
		std::cout << "\n" << "p2: " << p2 << "\n";
		print_array(std::cout, p2, ndsiz);
		std::cout << "\np2: " << p2 << "\np3: " << p3 << "\n";
		delete[] p2;
		delete p1;

		int* p11 = new int[10];
		for (int i = 0; i < 10; ++i)
		{
			p11[i] = i * 2;
		}
		p11[0] = 1;

		int* p22 = new int[10];
		for (int i = 0; i < 10; ++i)
		{
			p22[i] = p11[i];
		}
		vector<int> vp1(10);

		for (int i = 0; i < 10; ++i)
		{
			vp1.push_back(i * 2);
		}
		vp1[0] = 1;
		vector<int> vp2(10);
		vp2 = vp1;


		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}