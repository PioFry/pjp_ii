#include "std_lib_facilities.hpp"


class B1
{
public:
	virtual void vf() { std::cout << "B1::vf()\n"; }
	void f() { std::cout << "B1::f()\n"; }
	virtual void pvf() =0;

private:
};

class D1 : public B1
{
public:
	void vf() { std::cout << "D1::vf()\n"; }
	void f() { std::cout << "D1::f()\n"; }
private:
};

class D2 : public D1
{
public:
	void pvf() { std::cout << "D2:pvf()\n"; }
private:
};

class B2
{
public:
	virtual void pvf() = 0;
};

class D21 : public B2
{
public:
	void pvf() { std::cout << d21str << "\n"; }
private:
	std::string d21str = "D21 string";
};

class D22 : public B2
{
public:
	void pvf() { std::cout << D22i << "\n"; }
private:
	int D22i = 100;
};

void f(B2& b2)
{
	b2.pvf();
}

int main()
{

	/*B1 b;
	b.vf();
	b.f();
	D1 d1;
	d1.f();
	d1.vf();
	B1& bref(d1);
	bref.f();
	bref.vf();*/
	/*D2 d2;
	d2.f();
	d2.vf();
	d2.pvf();*/
	D21 d21;
	D22 d22;
	f(d21);
	f(d22);

	system("pause");
	return 0;
}