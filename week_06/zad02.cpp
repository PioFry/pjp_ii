#include "Graph.hpp"
#include "Simple_window.hpp"


class Immobile_circle : public Graph_lib::Shape
{
public:
	Immobile_circle(Point p, int rr);
	Immobile_circle() { };
	void draw_lines() const;

	Point center() const;
	int radius() const { return r; }
	void set_radius(int rr) { r = rr; }
private:
	int r;
	void move() { return; }
};

Immobile_circle::Immobile_circle(Point p, int rr)
	:r(rr)
{
	add(Point(p.x - r, p.y - r));       // store top-left corner
}

void Immobile_circle::draw_lines() const
{
	if (color().visibility())
		fl_arc(point(0).x, point(0).y, r + r, r + r, 0, 360);
}

Point Immobile_circle::center() const
{
	return Point(point(0).x + r, point(0).y + r);
}

class Stripped_rectangle : public Graph_lib::Rectangle
{
public:
	Stripped_rectangle(Point p, int h, int w, int sw);
	void draw_lines() const;
private:
	Graph_lib::Lines stripes;
	int stripes_width;
};

Stripped_rectangle::Stripped_rectangle(Point p, int h, int w, int sw)
	: Graph_lib::Rectangle{ p, w, h }, stripes_width{ sw }
{
	for (int i = p.y + sw; i < p.y + h; i += 2 * sw)
	{
		stripes.add({ p.x, i }, { p.x + (w - 1), i });
	}
	stripes.set_color(Graph_lib::Color::black);
}

void Stripped_rectangle::draw_lines() const
{
	Stripped_rectangle::stripes.draw();
	Graph_lib::Rectangle::draw_lines();
}

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window
	Simple_window win(tl, 800, 600, "My window");    // make a simple window
	
	Immobile_circle c1(Point{ 100, 100 }, 20);

	win.attach(c1);

	Stripped_rectangle sr1({ 200, 200 }, 50, 100, 3);
	win.attach(sr1);


	win.wait_for_button();       // give control to the display engine
}