#include "Graph.hpp"
#include "Simple_window.hpp"


int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window
	Simple_window win(tl, 600, 600, "Function Graphs");    // make a simple window
	
	int lenght = 400;
	int scale = 20;

	Graph_lib::Axis x{ Axis::x, Point {100, 300}, lenght, lenght / scale, "1 == 20 pixels" };
	win.attach(x);
	Graph_lib::Axis y{ Axis::y, Point {300, 500}, lenght, lenght / scale, "1 == 20 pixels" };
	win.attach(y);

	x.set_color(Color::red);
	y.set_color(Color::red);

	win.wait_for_button();       // give control to the display engine
}