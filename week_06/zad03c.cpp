#include "std_lib_facilities.hpp"


struct Person
{
private:
	std::string fname;
	std::string lname;
	int age;
	bool bad_person(Person p);
public:
	std::string first_name() const { return fname; }
	std::string last_name() const { return lname; }
	int get_age() const { return age; }
	Person(std::string first_name, std::string last_name, int years);
	Person() : age(0), fname(" "), lname(" ") { }
	friend std::istream& operator>>(istream& is, Person& p);
};

bool Person::bad_person(Person p)
{
	if (age > 150 || age < 0)
	{
		return true;
	}
	for (size_t i = 0; i < fname.size(); ++i)
	{
		//; : " ' [ ] * & ^ % $ # @ !
		switch (fname[i])
		{
		case ';':
		case ':':
		case '"':
		case '[':
		case ']':
		case '*':
		case '&':
		case '^':
		case '%':
		case '$':
		case '#':
		case '@':
		case '!':
			return true;
			break;
		default:
			break;
		}
	}
	for (size_t i = 0; i < lname.size(); ++i)
	{
		//; : " ' [ ] * & ^ % $ # @ !
		switch (lname[i])
		{
		case ';':
		case ':':
		case '"':
		case '[':
		case ']':
		case '*':
		case '&':
		case '^':
		case '%':
		case '$':
		case '#':
		case '@':
		case '!':
			return true;
			break;
		default:
			break;
		}
	}
	return false;
}

Person::Person(std::string first_name, std::string last_name, int years)
	: age(years), fname(first_name), lname(last_name)
{
	if (bad_person(*this))
	{
		error("Invalid person data");
	}
}

std::istream& operator>>(istream& is, Person& p)
{
	char ch2 = 0;
	
	if (is >> p.age >> ch2 >> p.fname >> p.lname)
	{
		if (ch2 != ',' || p.bad_person(p))
		{
			error("Invalid person data");
		}
		else
		{
			return is;
		}
	}
	else
	{
		error("Wrong input formatting");
	}
}

ostream& operator<<(ostream& os, Person& p)
{
	os << "First name: " << p.first_name() << "\n" << "Last name: " << p.last_name() << "\n" << "Age: "
		<< p.get_age() << "\n";
	return os;
}

int main()
{
	try
	{
		Person p1("Derp", "Derpstone", 63);
		Person p2;
		std::cout << "Frist person, age: " << p1.get_age() << " first name: " << p1.first_name()
			<< " last name: " << p1.last_name() << "\n";
		std::cin >> p2;		// "age" ',' "name" format
		//std::cout << "Second person age: " << p2.age << " name " << p2.name << "\n";
		std::cout << p2;
		Person p3;
		vector<Person> ppl;
		while (cin >> p3)
		{
			ppl.push_back(p3);
		}
		system("pause");
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
	return 0;
}