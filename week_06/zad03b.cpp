#include "Graph.hpp"
#include "Simple_window.hpp"

double one(double x)
{
	return 1;
}

double slope(double x)
{
	return x / 2;
}

double square(double x)
{
	return x*x;
}

double sloping_cos(double x) 
{ 
	return cos(x) + slope(x); 
}

int main()
{
	using namespace Graph_lib;   // our graphics facilities are in Graph_lib

	Point tl(100, 100);           // to become top left  corner of window
	Simple_window win(tl, 600, 600, "Function Graphs");    // make a simple window

	int lenght = 400;
	int scale = 20;
	constexpr double xscale = 20.0;
	constexpr double yscale = 20.0;

	Graph_lib::Axis x{ Axis::x, Point {100, 300}, lenght, lenght / scale, "1 == 20 pixels" };
	win.attach(x);
	Graph_lib::Axis y{ Axis::y, Point {300, 500}, lenght, lenght / scale, "1 == 20 pixels" };
	win.attach(y);

	Graph_lib::Function s(one, -10.0, 11.0, { 300, 300 }, 400, xscale, yscale);
	win.attach(s);
	Graph_lib::Function ss(slope, -10.0, 11.0, { 300, 300 }, 400, xscale, yscale);
	win.attach(ss);
	Graph_lib::Text ss_txt({ 100, 390 }, "x/2");
	win.attach(ss_txt);
	Graph_lib::Function sss(square, -10.0, 11.0, { 300, 300 }, 400, xscale, yscale);
	win.attach(sss);
	Graph_lib::Function cos(cos, -10.0, 11.0, { 300, 300 }, 400, xscale, yscale);
	win.attach(cos);
	Graph_lib::Function slope_cos(sloping_cos, -10.0, 11.0, { 300, 300 }, 400, xscale, yscale);
	win.attach(slope_cos);

	win.wait_for_button();       // give control to the display engine
}