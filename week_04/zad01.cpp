#include"std_lib_facilities.hpp"


class Point
{
public:
	Point(int xx, int yy) : x{ xx }, y{ yy } { std::cout << "constructing point\n"; };	//constructor
	Point() : x{ 0 }, y{ 0 } { std::cout << "constructing point\n"; }
	void print_point(Point p);
	int coordinate(char c);
private:
	int x, y;
};

int Point::coordinate(char c)
{
	if (c == 'x') {
		return x;
	}
	else if (c == 'y') {
		return y;
	}
	else
		error("wrong coordinate to get");
}

void Point::print_point(Point p)
{
	std::cout << "(" << p.x << ", " << p.y << ")";
}

istream& operator>>(istream& is, Point& p)
{
	int x, y;
	char ch1, ch2, ch3;
	is >> ch1 >> x >> ch2 >> y >> ch3;
	if (!is) return is;
	if (ch1 != '(' || ch2 != ',' || ch3 != ')') //if its true then input has wrong format
	{
		is.clear(ios_base::failbit); // set the fail bit
		return is;
	}
	p = Point(x, y);
	return is; //returning stream for "<<" chaining
}

ostream& operator<<(ostream& os, Point& p)
{
	return os << "(" << p.coordinate('x') << "," << p.coordinate('y') << ")";
}

bool operator==(Point& p1, Point& p2)
{
	return (p1.coordinate('x') == p2.coordinate('x')) && (p1.coordinate('y') == p2.coordinate('y'));
}

bool operator!=(Point& p1, Point& p2)
{
	return !(p1 == p2);
}

bool operator==(vector<Point>& v1, vector<Point>& v2)
{
	if (v1.size() == v2.size())
	{
		for (size_t i = 0; i < v1.size(); i++)
		{
			if (v1[i] == v2[i]) {
				continue;
			}
			else
				return false;
		}
		return true;
	}
	else
		return false;
}

bool operator!=(vector<Point>& v1, vector<Point>& v2)
{
	return !(v1 == v2);
}

void print_points(const vector<Point>& points)
{
	for (Point p : points)
	{
		std::cout << p << "\n";
	}
}

int main()
{
	vector<Point> original_points;
	vector<Point> processed_points;
	int n = 3;
	try
	{	//Open file for writting
		std::cout << "Please enter output file name: ";
		string oname;
		std::cin >> oname;
		ofstream ost{ oname }; // ost is an output stream for the file named name
		if (!ost) error("can't open output file ", oname);
		
		

		std::cout << "Please enter " << n << " points in (x, y) format: ";
		int i = 0;
		for (Point p; std::cin >> p; i++)
		{
			original_points.push_back(p);
			if (i == n - 1) {
				break;
			}
		}
		std::cout << "\n Out of loop\n";
		print_points(original_points);

		for (Point p : original_points)
		{
			ost << p << "\n";
		}

		ost.close();
		//if (ost) error("can't close output file ", oname);
		//open file for reading
		std::cout << "Please enter input file name: ";
		string iname;
		std::cin >> iname;
		ifstream ist { iname }; // ost is an output stream for the file named name
		if (!ost) error("can't open input file ", oname);
		for (Point p; ist >> p; )
		{
			processed_points.push_back(p);
		}
		print_points(processed_points);
		if(original_points != processed_points)
		{
			error("Something's wrong!");
		}
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}

	system("pause");
	return 0;
}
