#include"std_lib_facilities.hpp"



int main()
{
	try
	{
		char c;
		int num = 0, sum = 0;
		std::stringstream number;
		std::ifstream ist{ "tekst.txt" };

		while (ist.get(c))	//Iterate each character of file (until EOF)
		{
			if (isdigit(c))	//check if character is a digit
			{
				number.put(c);	//if so, put character in stream
				continue;	//skip everything that's below this line
			}
			else {
				ist.putback(c);	//put c back to stream
				ist.ignore(1);	//ignore 1 character from stream
			}
			number >> num;	//convert stream entity into integer (watch out!: it puts stream in EOF state)
			sum += num;
			num = 0;	//reset the integer
			number.str(std::string());	//empty stream
			number.clear();	//set stream state to good
		}
		number >> num;	//in case last character is a digit
		sum += num;
		std::cout << "Sum of all integers in file: " << sum << "\n";



	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
	system("pause");
	return 0;
}
