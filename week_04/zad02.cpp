//Following program takes 2 files with words space separated (in english alphabet) and writte it to third file 
//alphabetically sorted

#include"std_lib_facilities.hpp"


void fill_remaining_words(ofstream& ost , const vector<string>& words)
{
	for (std::string w : words)
	{
		ost << w << " ";
		std::cout << "Writting " << w << " to file\n";
	}
}

int main()
{
	try
	{
		//open file for reading
		string iname1;
		iname1 = "sw1.txt";
		ifstream ist1{ iname1 }; // ost is an output stream for the file named iname1
		if (!ist1) error("can't open first input file ", iname1);

		//open file for reading
		string iname2;
		iname2 = "sw2.txt";
		ifstream ist2{ iname2 }; // ost is an output stream for the file named iname2
		if (!ist2) error("can't open second input file ", iname2);

		//Open file for writting
		string oname;
		oname = "sw3.txt";
		ofstream ost{ oname }; // ost is an output stream for the file named oname
		if (!ost) error("can't open output file ", oname);

		stringstream ss;
		std::string word;
		vector<string> words1, words2;
		//filling vectors with words:
		while (ist1 >> word) {
			words1.push_back(word);
		}
		while (ist2 >> word) {
			words2.push_back(word);
		}
		//sorting start vectors
		sort(words1.begin(), words1.end()); //this is in case input file is not alphabetically sorted
		sort(words2.begin(), words2.end()); //this is in case input file is not alphabetically sorted
		//putting words into ost file in alphabetical order
		while (words1.size() > 0 && words2.size() > 0)
		{
			if (words1[0] <= words2[0])
			{
				std::cout << "Writting " << words1[0] << " to file\n";
				ost << words1[0] << " ";
				words1.erase(words1.begin()); //removes first element of vector
			}
			else
			{
				std::cout << "Writting " << words2[0] << " to file\n";
				ost << words2[0] << " ";
				words2.erase(words2.begin()); //removes first element of a vector
			}
		}
		if (words1.size() != 0)
		{
			fill_remaining_words(ost, words1);
		}
		else if (words2.size() != 0)
		{
			fill_remaining_words(ost, words2);
		}
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
	system("pause");
	return 0;
}
