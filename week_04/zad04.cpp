#include"std_lib_facilities.hpp"



int main()
{
	try
	{
		int birth_year = 1998;
		int a, b, c, d;
		std::cout << showbase << std::hex << "birth in hex: \t" << birth_year << "\n";
		std::cout << showbase << std::oct << "birth in octal: " << birth_year << "\n";
		std::cout << showbase << std::dec << "birth in dec: \t" << birth_year << "\n";

		std::cin >> a >> oct >> b >> hex >> c >> d; //input: 1234 1234 1234 1234 
		std::cout << a << '\t' << b << '\t' << c << '\t' << d << '\n'; //shows results of conversion from upgiven bases to decimal
		float myfl = 1234567.89;
		std::cout << defaultfloat << myfl << "\t" << fixed << myfl << "\t" << scientific << myfl;
		std::cout << "\n\n\n";


		std::cout << setw(19) << "Last name: " << setw(16) << "Frycki\n";
		std::cout << setw(19) << "First name: " << setw(16) << "Piotr\n";
		std::cout << setw(19) << "Telephone number: " << setw(16) << "604 229 001\n";
		std::cout << setw(19) << "Email: " << setw(16) << "masterju@o2.pl\n";
		std::cout << setw(19) << "Friend1: " << setw(16) << "Marcin\n";
		std::cout << setw(19) << "Friend2: " << setw(16) << "Rafal\n";
		std::cout << setw(19) << "Friend3: " << setw(16) << "Cichy\n";
		std::cout << setw(19) << "Friend4: " << setw(16) << "Slawek\n";
		std::cout << setw(19) << "Friend5: " << setw(16) << "Jakub\n";
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
	system("pause");
	return 0;
}
