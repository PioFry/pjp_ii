#include"std_lib_facilities.hpp"
#include<bitset>


int main()
{
	try
	{
		
		cin.showbase;

		int val;
		vector<int> vals;
		vector<string> str_vec;
		vector<string> prefixes;
		std::string mystr;
		std::stringstream ss;

		int no_of_ints = 7;
		std::cout << "Please enter " << no_of_ints << " integers: \n";
		for (int i = 0; i < no_of_ints; ++i)
		{
			std::cin >> mystr;
			str_vec.push_back(mystr);
			ss << mystr;
			if (mystr.front() == '0')
			{
				mystr.erase(mystr.begin(), mystr.begin() + 1);
				if (mystr.front() == 'x')
				{
					ss >> std::hex >> val;
					prefixes.push_back("hexadecimal");
				}
				else
				{
					ss >> std::oct >> val;
					prefixes.push_back("octal");
				}
			}
			else
			{
				ss >> std::dec >> val;
				prefixes.push_back("decimal");
			}
			vals.push_back(val);
			ss.clear();
		}
		for (size_t i = 0; i < vals.size(); ++i)
		{
			std::cout << setw(5) << str_vec[i] << " " << setw(13) << prefixes[i] << " converts to: " 
				<< setw(7) << vals[i] << "\n";
		}
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
	system("pause");
	return 0;
}
// 0xa1 2 0xa3 4 0xc5 6 0xb7
