#include "std_lib_facilities.hpp"


template<typename T, typename U>
auto dot_product(vector<T> vt, vector<U> vu)
{
	size_t siz;
	double sum = 0;
	if (vt.size() >= vu.size())
	{
		siz = vu.size();
	}
	else
	{
		siz = vt.size();
	}
	for (size_t i = 0; i < siz; ++i)
	{
		sum += vt[i] * vu[i];
	}
	return sum;
}

int main()
{
	try
	{
		vector<int> a = { 1, 2, 3 };
		vector<double> d = { 3.5, 2.0, 1.0 };
		std::cout << "Here it is: " << dot_product(a, d) << "\n";
		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}