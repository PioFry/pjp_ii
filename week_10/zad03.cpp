#include "std_lib_facilities.hpp"

template<typename T>
class Number
{
public:
	Number(int val) : i(val) { }
	Number() : i(0) { }
	void operator=(T v2);
	T value() const { return i; }
private:
	T i;
};

template<typename T>
void Number<T>::operator=(T v2)
{
	i = v2;
}

template<typename T>
ostream& operator<< (ostream& os, const Number<T>& in)
{
	os << in.value();
	return os;
}

template<typename T>
istream& operator>> (istream& is, Number<T>& in)
{
	T myt;
	if (is >> myt)
	{
		in = myt;
	}
	else
	{
		is.clear(is.bad());
		error("Istream insertion to class Int failed");
	}
	return is;
}

template<typename T>
Number<T> operator+ (Number<T> in1, Number<T> in2)
{
	Number<T> res;
	res = in1.value() + in2.value();
	return res;
}

template<typename T>
Number<T> operator- (Number<T> in1, Number<T> in2)
{
	Number<T> res;
	res = in1.value() - in2.value();
	return res;
}

template<typename T>
Number<T> operator* (Number<T> in1, Number<T> in2)
{
	Number<T> res;
	res = in1.value() * in2.value();
	return res;
}

template<typename T>
Number<T> operator/ (Number<T> in1, Number<T> in2)
{
	Number<T> res;
	res = in1.value() / in2.value();
	return res;
}

template<typename T>
Number<T> operator% (Number<T> in1, Number<T> in2)
{
	Number<T> res;
	res = fmod(in1.value(), in2.value());
	return res;
}

int main()
{
	try
	{
		Number<int> first(1);
		Number<int> second;
		Number<int> res;
		std::cout << "First: " << first << "\n" << "Second: " << second << "\n";
		second = first;
		std::cout << "First: " << first << "\n" << "Second: " << second << "\n";
		std::cin >> first >> second;
		std::cout << "First: " << first << "\n" << "Second: " << second << "\n";
		res = first + second;
		std::cout << "First + second: " << res << "\n";
		res = first - second;			  
		std::cout << "First - second: " << res << "\n";
		res = first * second;			  
		std::cout << "First * second: " << res << "\n";
		res = first / second;			  
		std::cout << "First / second: " << res << "\n";
		res = first % second;
		std::cout << "First % second: " << res << "\n";
		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}