#include "std_lib_facilities.hpp"


ostream& operator<<(ostream& os, vector<int> vec)
{
	for (size_t i = 0; i < vec.size(); ++i)
	{
		os << vec[i] << ", ";
	}
	return os;
}

template<typename T> 
struct S
{
	S(T a) : val(a) { }
	void print() { std::cout << get() << "\n"; }
	const T& c_get();
	T& get();
	void S<T>::operator=(const T& a);
	//void set(T a) { val = a; }
private:
	T val;
};

template<typename T>
void S<T>::operator=(const T& a)
{
	val = a;
}

template<typename T>
T& S<T>::get() 
{
	return val; 
}

template<typename T>
const T& S<T>::c_get()
{
	return val;
}

template<typename T> 
void read_val(T& v)
{
	if (!(std::cin >> v))
	{
		error("Wrong types");
	}
}

int main()
{
	try
	{
		S<int> myint(1);
		S<char> mychar('a'); 
		S<double> mydouble(1.2);
		S<string> mystr("test");
		S<vector<int>> myvec({ 1, 2, 3 });
		myint.print();
		mychar.print();
		mydouble.print();
		mystr.print();
		myvec.print();
		read_val(myint.get());
		read_val(mychar.get());
		read_val(mydouble.get());
		read_val(mystr.get());
		myint.print();
		mychar.print();
		mydouble.print();
		mystr.print();
		myvec.print();

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}


/*
if (x < (d + 1) / 2)	//recursion end term
{
fermat(static_cast<int> (2 * (x + temp)), result_tab);	//multiply by to 'cause we pass an 'a' and we want it to be 'b'
fermat(static_cast<int> (2 * (x - temp)), result_tab);	//multiply by to 'cause we pass an 'a' and we want it to be 'b'
}
else
{
return;
}
*/