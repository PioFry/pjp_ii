//exercise 13 page 340
/*
Problems list:
1. Assignment operator, default assignment operator works when rvalue and lvalue is Rational type variable, why,
to what and how to change it?
2. Not sure how operator overloading works, eg: Rational operator+ (Rational& r1)
2.1 Why do we need reference as argument?
2.2 '+' is usually between two rationals and we have only one rational as argument
2.3 How does operators work? Do they return a value instead of expression entered?
3. When should you make operators overloads member functions?

Ideas:
- Make Rational t = 2/2 short itself to 1 automatically. Same with 2/4 to 1/2
*/
#include "std_lib_facilities.hpp"


class Rational
{
public:
	void print_rat();
	double rat_value() { return (double(num) / denum); }
	Rational operator+(Rational& r1);
	Rational operator-(Rational& r1);
	Rational operator*(Rational& r1);
	Rational operator/(Rational& r1);
	bool operator==(Rational& r1);
	Rational() : num{ 1 }, denum{ 1 } { std::cout << "rational constructed\n"; }
	Rational(int n, int d);

private:
	int num;
	int denum;
};

Rational::Rational(int n, int d)
	: num{ n }, denum{ d }
{
	/*if (isdigit(n)) {											//Why !isdigit does not react on doubles?
	error("Wrong constructor arguments!");
	}*/
	std::cout << "rational constructed\n";
}

void Rational::print_rat()
{
	std::cout << num << "/" << denum << "\n";
}

Rational Rational::operator+(Rational& r1)
{
	Rational rat;
	if (denum == r1.denum) {
		rat.num = num + r1.num; //if denumerators are equal just add numerators
		rat.denum = denum;
	}
	else {
		rat.num = num * r1.denum; //else make denumerators equal and add numerators
		rat.denum = denum * r1.denum;
		rat.num = num + r1.num;
	}
	return rat;
}

Rational Rational::operator-(Rational& r1)
{
	Rational rat;
	if (denum == r1.denum) {
		rat.num = num - r1.num;	//if denumerators are equal just substract numerators
		rat.denum = denum;
	}
	else {
		rat.num = num * r1.denum; //else make denumerators equal and substract numerators
		rat.denum = denum * r1.denum;
		rat.num = num - r1.num;
	}
	return rat;
}

Rational Rational::operator*(Rational& r1)
{
	Rational rat;
	rat.num = num * r1.num;
	rat.denum = denum * r1.denum;
	return rat;
}

Rational Rational::operator/(Rational& r1)
{
	Rational rat;
	rat.num = num * r1.denum;
	rat.denum = denum * r1.num;
	return rat;
}

bool Rational::operator==(Rational& r1)
{
	return (num == r1.num) && (denum == r1.denum);
}

int main()
{
	try
	{
		Rational rat1(1, 2);
		Rational rat2(1, 2);
		Rational rat3 = (rat1 - rat2);
		rat3.print_rat();
		rat3 = (rat1 + rat2);
		rat3.print_rat();
		rat3 = (rat1 * rat2);
		rat3.print_rat();
		rat3 = (rat1 / rat2);
		rat3.print_rat();
		if (rat1 == rat2)
			std::cout << "is equal\n";
		rat1 == rat3 ? std::cout << "is equal\n": std::cout << "is different\n";
		std::cout << rat3.rat_value() << "\n";
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}


	system("pause");
	return 0;
}