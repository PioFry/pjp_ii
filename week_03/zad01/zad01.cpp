#include <conio.h>
#include "std_lib_facilities.hpp"

namespace X 
{
	int var;
	void print()
	{
		std::cout << "x\t" << var << "\n";
	}
}

namespace Y
{
	double var;
	void print()
	{
		std::cout << "y\t" << var << "\n";
	}
}

namespace Z
{
	char var;
	void print()
	{
		std::cout << "z\t" << var << "\n";
	}
}

int main()
{
	X::var = 7;
	X::print(); // print X�s var
	using namespace Y;
	var = 9;
	print(); // print Y�s var
	{ 
	using Z::var;
	using Z::print;
	var = 11;
	print(); // print Z�s var
	}
	print(); // print Y�s var
	X::print(); // print X�s var
	_getch();
	return 0;
}