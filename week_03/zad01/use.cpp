#include "my.h"
#include <conio.h>
#include <iostream>


int foo = 7;
int main()
{
	print_foo();
	print(5);

	int x = 7;
	int y = 9;
	swap_v (x, y); // replace ? by v, r, or cr
	swap_v (7, 9);
	std::cout << x << "\n" << y << "\n";
	const int cx = 7;
	const int cy = 9;
	swap_v (cx, cy);
	swap_v (7.7, 9.9);
	std::cout << cx << "\n" << cy << "\n";
	double dx = 7.7;
	double dy = 9.9;
	swap_v (dx, dy);
	swap_v (7.7, 9.9);
	std::cout << dx << "\n" << dy << "\n";
	/* Conclusion:
	- swap by value compile each time but doesn't change neither of values
	- swap by reference compiles only with variables (lvalues) as arguments (eg. 1st swap) and actually
	changes the values of variables (and non constants)
	- swap by const reference doesn't even compile but it seems it would accept same things as reference pass
	including const int
	*/


	_getch();
	return 0;
}