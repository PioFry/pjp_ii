#include "std_lib_facilities.hpp"
#include "my.h"


void print_foo()
{
	std::cout << foo << "\n";
}
void print(int i)
{
	std::cout << i << "\n";
}
void swap_v(int a, int b)
{
	int temp; 
	temp = a, a = b; b = temp;
}
void swap_r(int& a, int& b)
{
	int temp; 
	temp = a, a = b; b = temp;
}

// Here i leave commented function of swap because it doesn't compile but the drill forbids any changes,
// to functions body
//void swap_cr(const int& a, const int& b)
//{ 
//	int temp; 
//	temp = a, a = b; b = temp;
//}
