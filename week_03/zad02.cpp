#include <conio.h>
#include "std_lib_facilities.hpp"
#include <string>


vector<string> fill_names()
{
	vector<string> str_vec;
	std::cout << "enter names (press ctrl + z to stop)\n";
	string name;
	std::cout << "name: ";
	while (cin >> name)
	{
		str_vec.push_back(name);
		std::cout << "name: ";
	}
	cin.clear();
	cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');	//clears stream and ignores unnecesary signs
	return str_vec;													//so next cin can work properly
}
vector<double> fill_ages(const vector<string>& names)
{
	vector<double> age_vec;	//
	for (size_t i = 0; i < names.size(); ++i)
	{
		std::cout << "Please enter age to be assigned: ";
		double x;
		std::cin >> x;
		if(!cin)
			error("this is not valid age");
			age_vec.push_back(x);
	}
	return age_vec;
}

void print_names(const vector<string>& name_vec)
{
	for(string name : name_vec)
	{
		std::cout << "\n" << name << "\n";
	}
}

void print_ages(const vector<double>& age_vec)
{
	for (double age : age_vec)
	{
		std::cout << "\t" << age << "\n";
	}
}

void print_pair(const vector<string>& str_vec, const vector<double>& dbl_vec)
{
	if (str_vec.size() != dbl_vec.size())
		error("unequal number of names and ages");

	for (size_t i = 0; i < dbl_vec.size(); ++i)
	{
		std::cout << "(" << str_vec[i] << ", " << dbl_vec[i] << ")\n";
	}
}

vector<double> adjust_age(const vector<string>& copy, const vector<string>& sorted, const vector<double>& age)
{
	vector<double> adjusted(age.size());
	if (copy.size() != sorted.size())
		error("copied vector isn't same size as sorted vector");
	for (size_t i = 0; i < copy.size(); ++i)
	{
		for (size_t j = 0; j < sorted.size(); ++j)
		{
			if (copy[i] == sorted[j])	//if the same name is found 
				adjusted[j] = age[i]; //put age firstly assigned to that name in position found
		}
	}
	return adjusted;
}

int main()
{
	try
	{
		vector<string> names = fill_names();
		vector<string> names_cpy(names);	//copy of names
		vector<double> ages = fill_ages(names);
		print_pair(names, ages);
		sort(names);
		std::cout << "\n" << "adjusting age\n\n";
		ages = adjust_age(names_cpy, names, ages);
		print_pair(names, ages);
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}

	_getch();
	return 0;
}