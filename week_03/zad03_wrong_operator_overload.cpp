#include "std_lib_facilities.hpp"


enum class Month {
	jan = 1, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec
};

bool operator> (Month& m, int n)
{
	return (int(m) > n);
}

bool operator< (Month& m, int n)
{
	return (int(m) < n);
}


//int operator+ (Month& m, int n)
//{
//	return (int(m) + n);
//}



class Date {
public:
	// . . .
	int day() const { return d; } // const member: can�t modify the object
	Month month() const { return m; }  // const member: can�t modify the object
	int year() const { return y; } // const member: can�t modify the object
	void add_day(int n); // non-const member: can modify the object
	void add_month(int n); // non-const member: can modify the object
	void add_year(int n); // non-const member: can modify the object
	Date::Date(int yy, Month mm, int dd)
		:y{ yy }, m{ mm }, d{ dd }
	{
		if (d > 31 || d < 0)
			error("wrong day\n");
		if (m > 12 || m < 0)
			error("wrong month");
		std::cout << "constructed date\n";
	}
private:
	//int operator= (Month m)
	//{
	//	return int(m);
	//}
	int y; // year
	Month m;
	int d; // day of month
};


ostream& operator<<(ostream& os, Month m)
{
	return os << int(m);
}

ostream& operator<<(ostream& os, const Date& d)
{
	return os << '(' << d.year() << ',' << d.month() << ',' << d.day() << ')';
}

void Date::add_day(int n)
{
	if ((d + n) > 31)
	{
		d = (d + n) - 31;
		add_month(1);	// now it doesn't do anything but it should
	}
	else if (n < 0)
		error("No negative days");
	else
		d += n;
}
void Date::add_month(int n)
{
//	m = m + n;
}
void Date::add_year(int n)
{
	y += n;
}

int main()
{
	try
	{
		Date today(2004, Month::dec, 31);
		Date tomorrow(today);
		tomorrow.add_day(-1);
		std::cout << today << "\n";
		std::cout << tomorrow << "\n";
		/*std::cout << today.month() << "\n";
		today.add_month(2);
		std::cout << today.month() << "\n";
		today.add_month(11);
		std::cout << today.month() << "\n";*/
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << "\n";
	}
	

	system("pause");
	return 0;
}