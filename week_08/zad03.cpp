#include<regex>
#include<iostream>
#include<fstream>
#include<string>


int main()
{
	try
	{
		std::ifstream ifs("My_mail_file.txt");
		std::smatch matches;
		std::string my_str;
		std::regex reg{ R"(^.+$)" };
		for (;!(ifs.eof());)
		{
			getline(ifs, my_str);
			std::cout << "\tmystr: " << my_str << "\n";
			std::regex_search(my_str, matches, reg);
			std::cout << "contents of matches: " << matches[0] << "\n";
			std::cout << "matches size: " << matches.size() << "\n";
		}
		std::string s1 = "\n";
		std::regex_search(s1, matches, reg);
		std::cout << "contents of matches: " << matches[0] << "\n";
		std::cout << "matches size: " << matches.size() << "\n";
		std::regex_match(s1, matches, reg);
		std::cout << "contents of matches: " << matches[0] << "\n";
		std::cout << "matches size: " << matches.size() << "\n";
	//Getline add additional "\" before \n so then regex maches it
	//When u hardcode "\n" as string then . operator reads none such a thing!

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}
