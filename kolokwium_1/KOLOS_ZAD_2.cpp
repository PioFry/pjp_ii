#include "std_lib_facilities.hpp"
#include <bitset> //bitset<>
#include <iomanip> // fill()
#include <algorithm> // reverse()


struct A
{
	virtual void f1() const { std::cout << "this is A::f1()\n"; }
	void f2() 
	{ 
		std::cout << "this is A::f2()\n" << "now will call A::f1()\n";
		this->A::f1();	//odp pod kodem
		std::cout << "this is still A::f2() now will call f1()\n";
		f1();
	}
};

struct B : public A
{
	virtual void f1() const override 
	{
		std::cout << "this is B::f1()\n";
	};
};

void test(A& a)
{
	std::cout << "### this is test() now will call virtual a.f1()\n";
	a.f1();
	std::cout << "### this is still test() now will call NON-virtual a.f2()\n";
	a.f2();
}

int main()
{
	
	try
	{
		
		B b;
		test(b);

		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}
/* 
Uzycie this nie jest konieczne poniewaz odwolujemy sie do funkcji wewnatrz A, nie instancji A jako calosci
this jest potrzebne gdy chcemy zadzia�a� np jak�� funkcj� na ca�� instancj� klasy. 
np:
1. Gdy chcemy operowac na wskazniku do danej klasy.
2. Gdy chcemy rekursywnie zadzialac na member function nie przyjmujacy *this jako argument

void SomeFunc::przyklad()
{
	** some stuff **
	this->przyklad()
}
*/