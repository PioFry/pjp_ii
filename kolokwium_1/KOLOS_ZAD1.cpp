#include "std_lib_facilities.hpp"
#include <bitset> //bitset<>
#include <iomanip> // fill()
#include <algorithm> // reverse()


class NamePairs
{
public:
	void load(std::string a);
	void save(std::string a);
	void readNames();
	void readAges();
	void print() const;
	vector<std::string> get_names() const { return name; }
	vector<double> get_ages() const { return age; }
	void sort();
private:
	vector<std::string> name;
	vector<double> age;
};

void NamePairs::readNames()
{
	std::string names;
	std::cout << "Please enter names ('|' sign to stop): ";
	for (; std::cin >> names;)
	{
		if (names == "|")
		{
			break;
		}
		name.push_back(names);
	}
}

void NamePairs::readAges()
{
	if (name.size() == 0)
	{
		error("No names to assign ages to");
	}
	double ages;
	std::cout << "Please enter age for each name accordingly: ";
	for (size_t i = 0; i < name.size(); i++)
	{
		if (std::cin >> ages)
		{
			age.push_back(ages);
		}
		else
		{
			error("Wrong age");
		}
	}
}

void NamePairs::print() const
{
	if (name.size() != 0)
	{
		for (size_t i = 0; i < name.size(); ++i)
		{
			std::cout << "(" << name[i] << ", ";
			if (i < age.size())
			{
				std::cout << age[i] << ")" << "\n";
			}
			else
			{
				error("No age for a name!");
			}
		}
	}
	else
	{
		std::cout << "No name - age pairs to print.\n";
	}
}

ostream& operator<< (ostream& os, NamePairs& np)
{
	for (size_t i = 0; i < np.get_names().size(); i++)
	{
		os << "(" << np.get_names()[i] << ", " << np.get_ages()[i] << ")\n";
	}
	return os;
}

void NamePairs::sort()
{
	std::string temp_name;
	double temp_age;
	for (;;)
	{
		int counter = 0;
		for (size_t i = 0; i < (name.size() - 1); i++)
		{
			if (name[i] > name[i + 1])
			{
				counter += 1;
				temp_name = name[i];
				name[i] = name[i + 1];
				name[i + 1] = temp_name;

				temp_age = age[i];
				age[i] = age[i + 1];
				age[i + 1] = temp_age;
			}
		}
		if (counter == 0)
		{
			break;
		}
	}
}

void NamePairs::load(std::string a)
{
	char e, b, c;
	std::string ne;
	double ae;
	std::ifstream ifs(a);
	for (; ifs >> e >> ne >> b >> ae >> c ;)
	{
		if (ifs)
		{
			name.push_back(ne);
			age.push_back(ae);
		}
		else
		{
			error("Wrong read format in file");
		}
	}
}

void NamePairs::save(std::string a)
{
	std::ofstream ofs(a);
	ofs << this;
}

int main()
{
	
	try
	{
		NamePairs test;
		std::string choice;
		std::string file;
		
		std::cout << "Do you want to read from file? (y/n): ";
		std::cin >> choice;
		if (choice == "y")
		{
			std::cout << "File name: ";
			std::cin >> file;
			test.load(file);
		}
		else
		{
			test.readNames();
			test.readAges();
			test.print();
			std::cout << "\n";
			test.sort();
			test.print();
		}
		std::cout << "Do you want to save results? (y/n): ";
		std::cin >> choice;
		if (choice == "y")
		{
			std::cout << "Enter file name: ";
			std::cin >> file;
			test.save(file);
		}
		system("pause");
		return 0;
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << "\n";
		system("pause");
	}
}