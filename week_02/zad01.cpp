#include "std_lib_facilities.hpp"

int main() {
	try {
		std::cout << "1\t";
		cout << "Success!\n";
		std::cout << "2\t";
		cout << "Success!\n";
		std::cout << "3\t";
		cout << "Success" << "!\n";
		std::cout << "4\t";
		cout << "Success" << "!\n";

		std::cout << "5\t";
		string res = "7";
		vector<string> v20(10);
		v20[5] = res;
		cout << "Success!\n";

		std::cout << "6\t";
		vector<int> v1(10);
		v1[5] = 7;
		if (v1[5] == 7) cout << "Success!\n";

		std::cout << "7\t";
		bool cond = true;
		if (cond) cout << "Success!\n";
		else cout << "Fail!\n";

		std::cout << "8\t";
		bool c = true;
		if (c) cout << "Success!\n";
		else cout << "Fail!\n";

		std::cout << "9\t";
		string s = "ape";
		bool c1 = "fool" > s; //comparing strings returns a bool value
		if (c1) cout << "Success!\n";

		std::cout << "10\t";
		string s1 = "fool";		if (s1 == "fool") cout << "Success!\n";
		std::cout << "11\t";
		string s2 = "ape";
		if (s2 == "ape") cout << "Success!\n";

		std::cout << "12\t";
		string s3 = "ape";
		if (s3 < "fool") cout << "Success!\n";

		std::cout << "13\t";
		vector<char> v2(5);
		for (int i = 0; i < v2.size(); ++i)
			cout << "Success!\n";

		std::cout << "14\t";
		vector<char> v3(5);		for (int i = 0; i < v3.size(); ++i)			cout << "Success!\n";
		std::cout << "15\t";
		string s4 = "Success!\n";		for (int i = 0; i<6; ++i)			cout << s4;
		std::cout << "16\t";
		if (true)
			cout << "Success!\n";
		else
			cout << "Fail!\n";

		std::cout << "17\t";
		int x20 = 2000;
		int cc203 = x20;
		if (cc203 == 2000)
			cout << "Success!\n";

		std::cout << "18\t";
		string s20 = "Success!\n";
		for (int i = 0; i<10; ++i)
			cout << s20;

		std::cout << "19\t";
		vector<int> v(5);
		for (int i = 0; i <= v.size(); ++i)
			cout << "Success!\n";

		std::cout << "20\t";
		int i1 = 0;
		int j = 9;
		while (i1<10)
			++i1;
		if (j<i1)
			cout << "Success!\n";

		std::cout << "21\t";
		int x21 = 3;
		double d55 = 5.0 / (x21 - 2);
		if ((d55 - 4.5) == 0.5)
			cout << "Success!\n";

		std::cout << "22\t";
		string s7 = "Success!\n";
		for (int i = 0; i <= 10; ++i)
			cout << s7;

		std::cout << "23\t";
		int i4 = 0;
		int j5 = 5;
		while (i4<10)
			++i4;
		if (j5<i4)
			cout << "Success!\n";

		std::cout << "24\t";
		int x = 4;
		double d = 5.0 / (x - 2);
		if (d == 2 + 0.5)
			cout << "Success!\n";

		std::cout << "25\t";
		cout << "Success!\n";


		system("pause");
		return 0;
	}
	catch (exception& e) {
		cerr << "error: " << e.what() << '\n';
		return 1;
	}
	catch (...) {
		cerr << "Oops: unknown exception!\n";
		return 2;
	}
}