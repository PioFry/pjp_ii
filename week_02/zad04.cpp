#include "std_lib_facilities.hpp"
#include <cstdlib>
#include <string>



int factorial(int number)
{
	if (number < 0 || number > INT_MAX)
	{
		cerr << "Can't compute factorial of negative numbers\n";
		system("pause");
		std::exit(EXIT_FAILURE);
	}
	int fac = 1;
	for (int i = 1; i <= number; ++i)
	{
		fac *= i;
	}
	return fac;
}
//computates all permutations of number_used from number_pool of numbers.
double permutation(int number_pool, int numbers_used)
{
	if (number_pool - numbers_used < 0)
	{
		cerr << "First integer can't be smaller than second one!\n";
		system("pause");
		std::exit(EXIT_FAILURE);
	}
	double perm;
	perm = factorial(number_pool) / factorial((number_pool - numbers_used));
	return perm;
}
//same as permutation but it doesn't include repetition (a : b) (b : a).
double combination(int number_pool, int numbers_used)
{
	double comb;
	comb = permutation(number_pool, numbers_used) / factorial(numbers_used);
	return comb;
}

int main()
{
	int num1, num2;
	string choice;
	std::cout << "please enter two integer numbers: ";
	std::cin >> num1;
	std::cin >> num2;
	if (!cin)
	{
		cerr << "This clearly is not integer value!\n";
		system("pause");
		std::exit(EXIT_FAILURE);
	}
	std::cout << "Enter 'perm' if u want to calculate permutation of numbers\n";
	std::cout << "else, enter 'comb' to computate combination of numbers\n";
	std::cin >> choice;
	std::cout << "\n";
	if (choice == "perm")
		std::cout << permutation(num1, num2) << std::endl;
	else if (choice == "comb")
		std::cout << combination(num1, num2) << std::endl;
	else
		std::cout << "I do not recognize this command\n";
	system("pause");
	return 0;
}