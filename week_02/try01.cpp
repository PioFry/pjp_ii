#include "std_lib_facilities.hpp"



int area(int length, int width) // calculate area of a rectangle
{
	if (length <= 0 || width <= 0) error("non-positive area() argument");
	return length*width;
}

int framed_area(int x, int y) // calculate area within frame
{
	constexpr int frame_width = 2;
	if (x - frame_width <= 0 || y - frame_width <= 0)
		error("non-positive area() argument called by framed_area()");
	return area(x - frame_width, y - frame_width);
}

void f(int x, int y, int z)
{
	try
	{
		int area1 = area(x, y);
		std::cout << area1 << "\n";
		if (area1 <= 0) error("non-positive area");
		int area2 = framed_area(x, z);
		std::cout << area2 << "\n";
		int area3 = framed_area(y, z);
		std::cout << area3 << "\n";
		double ratio = double(area1) / area3;
		std::cout << ratio << "\n";
		// . . .
	}
	catch (exception& e)
	{
		cerr << e.what() << '\n';
		keep_window_open("~~");
		return;
	}

}

int main()
{
	std::cout << "enter 3 integers\n";
	int x, y, z;
	std::cin >> x >> y >> z;
	f(x, y, z);
	system("pause");
	return 0;
}