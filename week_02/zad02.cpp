#include "std_lib_facilities.hpp"
#include "std_lib_facilities.hpp"
#include <string>


int main()
{
	vector<int> myvect;
	int value;
	int sum = 0;
	for (;;)
	{
		std::cout << "Please enter the number of values you want to sum: ";
		int N = 0;
		std::cin >> N;
		if (N == 0)
		{
			cerr << "Wrong input";
			system("pause");
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		std::cout << "Please enter some integers (press '|' to stop): ";
		
		while (std::cin >> value)
			myvect.push_back(value);

		if (int(myvect.size()) < N)
		{
			cerr << "You entered too little integers\n";
			system("pause");
			cin.clear();
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			continue;
		}

		
		for (int i = 0; i < N; i++)
		{
			sum += myvect[i];
		}
		std::cout << "The sum of the first " << N << " numbers ( ";
		for (int i = 0; i < N; i++)
		{
			std::cout << myvect[i] << " ";
		}
		std::cout << ") is: " << sum << "\n";
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}
	system("pause");
	return 0;
}