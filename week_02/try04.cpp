// calculate area of a rectangle;
// pre-conditions: length and width are positive
// post-condition: returns a positive value that is the area
#include "std_lib_facilities.hpp"

int area(int length, int width)
{
	try
	{
		if (length <= 0 || width <= 0) error("area() pre-condition");
		int a = length*width;
		if (a <= 0) error("area() post-condition");
		return a;
	}
	catch (exception& e)
	{
		cerr << "error: " << e.what() << '\n';
		keep_window_open();
		return -1;
	}
	
}
int main()
{
	int x = 2;
	int y = INT_MAX;
	area(x, y);

	system("pause");
	return 0;
}