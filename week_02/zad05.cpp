// Zadanie 5: wykonac Drill ze str. 251 (calculator08buggy.cpp)

/*
calculator08buggy.cpp

Helpful comments removed.

We have inserted 3 bugs that the compiler will catch and 3 that it won't.

changes history:
1 - name to quit in return token line 79
2 - missing name of char in token_stream line 32
3 - set char initialization to 0 token_stream::ignore() line 99
4 - vector loop possibly running out of range in set_value() line 123
6 - in declaration() magic constant used instead of symbolic constant line 214
7 - missing return d in primary line 153
8 - in primary error messedge paranthesis '(' changed to ')' line 155
9 - added define_name() function for predefined variables line 214
*/
#include "std_lib_facilities.hpp"

struct Token {
	char kind;
	double value;
	string name;
	Token(char ch) :kind(ch) { }	// initialize kind with ch
	Token(char ch, double val) :kind(ch), value(val) { }	// initialize kind and value
	Token(char ch, string n) :kind{ ch }, name{ n } {}	//initialize kind and name
};

class Token_stream {
	bool full; // Is buffer full
	Token buffer;
public:
	Token_stream() :full(0), buffer(0) { } // constructor for token_stream (initializes full to 0-flase).

	Token get();
	void unget(Token t) { buffer = t; full = true; } // return token to buffer

	void ignore(char c); // ignores chars until specified char occured
};

const char let = 'L';  // declaration token (for declaring variables)
const char function = 'f';
const char quit = 'Q';
const char print = ';';
const char number = '8';
const char name = 'a';	// name token

Token Token_stream::get()
{
	if (full) { full = false; return buffer; } // returns buffer value emptying buffer
	char ch;
	cin >> ch;
	switch (ch) {
	case '(':
	case ')':
	case '+':
	case '-':
	case '*':
	case '/':
	case '%':
	case ';':
	case '=':
	case ',':		// added for function use eg: pow(x , i)
		return Token(ch);  // let each token represent itself
	case '#':
		return Token(let); // added for let to '#' conversion
	case '.':
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
	{
		cin.unget();
		double val;
		cin >> val;
		return Token(number, val);	//Return token as number
	}
	default:
		if (isalpha(ch)) // checks if ch is a letter
		{
			string s;
			s += ch;
			while (cin.get(ch) && (isalpha(ch) || isdigit(ch))) s += ch; // checks if ch is letter or digit
			cin.unget();
			if (s == "let") return Token(let);	// starts variable token creation process
			if (s == "exit") return Token(quit); //returns quit token to quit program
			if (s == "function") return Token(function); // returns function token to use a function
			return Token(name, s);
		}
		error("Bad token");
	}
}

void Token_stream::ignore(char c)
{
	if (full && c == buffer.kind) //if end char is in buffer ignore just buffer and empty buffer
	{
		full = false;
		return;
	}
	full = false;	//if end char is not in buffer empty buffer 

	char ch = 0;
	while (cin >> ch) // look for input until char c occured;
		if (ch == c) return;
}

struct Variable {
	string name;
	double value;
	Variable(string n, double v) :name(n), value(v) { } // Variable constructor
};

vector<Variable> names; // vector to store variables in "memory of calculator

						//gets value from a variable with name s
double get_value(string s)
{
	for (size_t i = 0; i<names.size(); ++i) // search for name s in vector
		if (names[i].name == s) return names[i].value; //if found return s' value
	error("get: undefined name ", s);
}

void set_value(string s, double d)
{
	for (size_t i = 0; i < names.size(); ++i) //search through vector
		if (names[i].name == s)
		{
			names[i].value = d;	// if name s found assign value d to that variables' value
			return;
		}
	error("set: undefined name ", s);
}

bool is_declared(string s)	// checks if variable is already declared
{
	for (size_t i = 0; i<names.size(); ++i)
		if (names[i].name == s) return true;
	return false;
}

Token_stream ts;

double expression();

double primary()
{
	Token t = ts.get();	// get next token
	switch (t.kind) {	// if next token is ( check if its ( expression ) format
	case '(':
	{
		double d = expression();
		t = ts.get();
		if (t.kind != ')') error("')' expected");
		return d;
	}
	case '-':
		return -primary();
	case number:
		return t.value;
	case name:	// if its variable return this variables' value
		return get_value(t.name);
	default:
		error("primary expected");
	}
}

double term()
{
	double left = primary();
	while (true) {
		Token t = ts.get();	//get next token
		switch (t.kind) {
		case '*':
			left *= primary();
			break;
		case '/':
		{
			double d = primary();
			if (d == 0) error("divide by zero");
			left /= d;
			break;
		}
		default:
			ts.unget(t);
			return left;
		}
	}
}

double expression()
{
	double left = term();
	while (true)
	{
		Token t = ts.get(); // get next token
		switch (t.kind)
		{
		case '+':
			left += term();
			break;
		case '-':
			left -= term();
			break;
		default:
			ts.unget(t);
			return left;
		}
	}
}

double define_name(string var, double val) //for predefined constants creation
{
	if (is_declared(var))
		error(var, " is already declared");
	names.push_back(Variable(var, val));
	return val;
}

double declaration() //creates new variable
{
	Token t = ts.get(); // get next token
	if (t.kind != name) error("name expected in declaration");
	string name = t.name; // set of variable name
	if (is_declared(name)) error(name, " declared twice");
	Token t2 = ts.get(); // get next token
	if (t2.kind != '=') error("= missing in declaration of ", name);
	double d = expression(); // set variable value to expression after '=' token
	names.push_back(Variable(name, d)); // add variable to vector (memory)
	return d; // print out value of variable 
}

double get_function() //uses one of defined functions
{
	Token t = ts.get(); //get next token
	if (t.kind != name) error("expected name in function");
	if (t.name == "sqrt") //computate square root of d
	{
		double d = expression();
		if (d < 0 || d > INT_MAX) error("can't calculate square root of negative value");
		return sqrt(d);
	}
	else if (t.name == "pow") // Multiply x with itself i times
	{
		Token t2 = ts.get();
		if (t2.kind != '(') error("expected a '('");
		double x = expression();
		Token t3 = ts.get();
		if (t3.kind != ',') error("expected a ','");
		int i = narrow_cast<int>(primary());	// narrow_cast<int> ensures that i is an int
		Token t4 = ts.get();
		if (t4.kind != ')') error("expected a ')'");
		return pow(x, i);
	}
	else error("undefined function");
}

double statement() // check if we are setting variable or calculating expression
{
	Token t = ts.get(); // get next token
	switch (t.kind)
	{
	case let:
		return declaration();
		break;
	case function:
		return get_function();
	default:
		ts.unget(t);
		return expression();
	}
}

void clean_up_mess() // recovers from error
{
	ts.ignore(print); // ignores all input until ';' is encountered
}

const string prompt = "> ";
const string result = "= ";

void calculate()
{
	while (true) try
	{
		cout << prompt;
		Token t = ts.get(); // get next token
		while (t.kind == print) t = ts.get(); // "eat" all ';' tokens
		if (t.kind == quit) return;
		ts.unget(t); // return token to buffer
		cout << result << statement() << endl;
	}
	catch (runtime_error& e)
	{
		cerr << e.what() << endl;
		clean_up_mess();
	}
}

int main()

try
{
	define_name("k", 1000);
	calculate();
	return 0;
}
catch (exception& e)
{
	cerr << "exception: " << e.what() << endl;
	char c;
	while (cin >> c&& c != ';');
	return 1;
}
catch (...)
{
	cerr << "exception\n";
	char c;
	while (cin >> c && c != ';');
	return 2;
}
/*
Input for testing:
1+1; 2+2; 2*2; 2/2; - 1 + 2 - 1; 2 * (2 + 2); 0 / 0; 1 / * - 2; 1 2 * 2 3+3 105; (2 2+2 * 2); @@4! 2+2;
let var = asdfsa; let 124 = 2; let; ;; 2 + quit; quit 2 + 2; -9999999999999999999999999999999 * 99999;
*/