#include "std_lib_facilities.hpp"

/*
Good values for low_temp should be -9227.4 which is zero absolute (impossible to get lower temp)
high temp is 134F which is highest record of temperature on earth
*/

int main()
{

	vector<double> temps; // temperatures
	for (double temp; cin >> temp; ) // read and put into temps
		temps.push_back(temp);

	double sum = 0;
	double high_temp = 134;
	double low_temp = -9227.4;
	for (int x : temps)
	{
		if (x > high_temp) high_temp = x; // find high		//Bad conversions from double to int...
		if (x < low_temp) low_temp = x; // find low
		sum += x; // compute sum
	}

	cout << "High temperature: " << high_temp << '\n';
	cout << "Low temperature: " << low_temp << '\n';
	cout << "Average temperature: " << sum / temps.size() << '\n';
	system("pause");
	return 0;
}