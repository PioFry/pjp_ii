#include "std_lib_facilities.hpp"
#include <vector>


bool is_unit_correct(string unit, vector<string> allowed_units)
{
	bool correct_unit = false;
	for (string i : allowed_units)
	{
		if (unit == i)
			correct_unit = true;
	}
	return correct_unit;
}
void print_vector(vector<double> vector_to_print)
{
	sort(vector_to_print);
	for (double x : vector_to_print)
	{
		std::cout << "\n" << x << " m" << "\n";
	}
}

int main()
{
	double sum = 0;
	double current;
	double current_in_cm = 0;
	double last_smallest = 0;
	double last_largest = 0;
	int counter = 0;
	vector<string> my_units = { "cm", "m", "in", "ft" };
	vector<double> values_entered;
	string unit = " ";
	std::cout << "Enter number followed by unit: ";
	while (std::cin >> current >> unit)
	{
		if (!is_unit_correct(unit, my_units))
			simple_error("You baddie! This is not a correct unit.");

		std::cout << current << unit;
		//bringing number down to cm for comparison of numbers;
		if (unit == "m")
			current_in_cm = current * 100;
		if (unit == "in")
			current_in_cm = current * 2.54;
		if (unit == "ft")
			current_in_cm = current * 30.48;
		if (unit == "cm")
			current_in_cm = current;
		if (counter == 0)
		{
			std::cout << " - this is your first number! \n";
			last_smallest = current_in_cm;
			last_largest = current_in_cm;
		}
		if (current_in_cm < last_smallest && counter != 0)
		{
			std::cout << " - the smallest so far \n";
			last_smallest = current_in_cm;
		}
		if (current_in_cm > last_largest && counter != 0)
		{
			std::cout << " - the largest so far \n";
			last_largest = current_in_cm;
		}
		sum += current_in_cm * 0.01;
		values_entered.push_back(current_in_cm * 0.01);
		counter += 1;
	}
	std::cout << "\n\n\n" << "largest of all: " << (last_largest * 0.01) << ", " << "smallest of all: " << (last_smallest * 0.01);
	std::cout << ", " << "number of values entered: " << counter << ", " << "sum of values: " << sum << "\n";

	print_vector(values_entered);

	system("pause");
	return 0;
}