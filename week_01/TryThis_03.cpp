#include "std_lib_facilities.hpp"


int main()
{
	constexpr double yen_to_dollar = 0.009038; 
	constexpr double pound_to_dollar = 1.30695;
	constexpr double euro_to_dollar = 1.134378;
	double amount = 1;
					 
	string unit = " "; 
		cout << "Please enter a amount of money followed by a unit (yen, pd, eu):\n";
	cin >> amount >> unit;
	if (unit == "yen")
		cout << amount << "yen == " << yen_to_dollar*amount << " dollars\n";
	else if (unit == "pd")
		cout << amount << "pd == " << amount * pound_to_dollar << " dollars\n";
	else if (unit == "eu")
		std::cout << amount << "eu == " << amount * euro_to_dollar << " dollars\n";
	else
		cout << "Sorry, I don't know a unit called '" << unit << "'\n";
	system("pause");
	return 0;
}