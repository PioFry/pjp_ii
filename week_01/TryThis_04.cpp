#include "std_lib_facilities.hpp"


int main()
{
	constexpr double yen_to_dollar = 0.009038; 
	constexpr double pound_to_dollar = 1.30695;
	constexpr double euro_to_dollar = 1.134378;
	constexpr double yuan_to_dollar = 0.148938;
	constexpr double kroner_to_dollar = 0.116225;
	double amount = 1;
					 
	char unit = ' '; 
		cout << "Please enter a amount of money followed by a unit (y, p, e, u, k):\n";
	cin >> amount >> unit;
	switch (unit)
	{
	case 'y':
			cout << amount << "y == " << yen_to_dollar*amount << " dollars\n";
			break;
	case 'p':
			cout << amount << "p == " << amount * pound_to_dollar << " dollars\n";
			break;
	case 'e':
			std::cout << amount << "e == " << amount * euro_to_dollar << " dollars\n";
			break;
	case 'u':
		    std::cout << amount << "u == " << amount * yuan_to_dollar << " dollars\n";
			break;
	case 'k':
			std::cout << amount << "k == " << amount * kroner_to_dollar << " dollars\n";
			break;
	default:
			cout << "Sorry, I don't know a unit called '" << unit << "'\n";
			break;
	}
	system("pause");
	return 0;
}