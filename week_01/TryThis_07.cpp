#include "std_lib_facilities.hpp"


int square(int value)
{
	int x = 0;
	for (int i = 0; i < value; ++i)
	{
		x += value;
	}
	return x;
}

int main()
{
	int x;
	std::cin >> x;
	std::cout << square(x) << "\n";
	std::cout << "Square of 2 and 10 :\n";
	cout << square(2) << '\n';
	cout << square(10) << '\n';
	system("pause");
	return 0;
}