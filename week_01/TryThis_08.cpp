#include "std_lib_facilities.hpp"

bool word_permitted(string word, vector<string> banned_words)
{
	bool permission = true;
	int i = 0;
	for (string x : banned_words)
	{
		if (word == banned_words[i])
			permission = false;
		++i;
	}
	return permission;
}

int main()
{
	vector<string> disliked_words = { "broccoli", "programming", "parufka" };
	vector<string> words;
	for (string temp; cin >> temp; ) // read whitespace-separated words
		if (!word_permitted(temp, disliked_words))
			words.push_back("Bleep");
		else
			words.push_back(temp); // put into vector
	cout << "Number of words: " << words.size() << '\n';
	sort(words); // sort the words

	for (int i = 0; i<words.size(); ++i)
		if (i == 0 || words[i - 1] != words[i]) // is this a new word?
			cout << words[i] << "\n";
	system("pause");
	return 0;
}