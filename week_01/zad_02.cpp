#include "std_lib_facilities.hpp"


int main()
{
	string first_name;
	string friend_name;
	char friend_sex = 0;
	int age;
	std::cout << "Enter the name of the person you want to write to: ";
	std::cin >> first_name;
	std::cout << "Dear " << first_name << "," << "\n";
	std::cout << "How are you? I am fine. I miss you " << "lorem ipsum, this is not what i asked for\n";
	std::cout << "Enter friend name: ";
	std::cin >> friend_name;
	std::cout << "Have you seen " << friend_name << " lately?" << "\n";
	std::cout << "Enter friends' sex (f-female, m-male): ";
	std::cin >> friend_sex;
	if (friend_sex == 'm')
		std::cout << "If you see " << friend_name << " please ask him to call me.\n";
	if (friend_sex == 'f')
		std::cout << "If you see " << friend_name << " please ask her to call me.\n";
	std::cout << "Enter your age: ";
	std::cin >> age;
	if (age <= 0 || age >= 110)
		simple_error("You're kidding!");
	else
		std::cout << "I hear you just had a birthday and you are " << age << " years old\n";
	if (age <= 12)
		std::cout << "Next year you will be" << (age + 1) << "\n";
	else if (age == 17)
		std::cout << "Next year you will be able to vote.\n";
	else if (age >= 70)
		std::cout << "I hope you're enjoying your retirement\n";
	std::cout << "Yours sincerely" << "\n\n" << "Peter\n";
	system("pause");
	return 0;
}