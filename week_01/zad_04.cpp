#include "std_lib_facilities.hpp"


int main()
{
	vector<int> computer_playstyle = { 1, 2, 3 };
	int choice_value = 0;
	int fight_resolve = -123;
	int i = 0;
	string choice = " ";
	std::cout << "Rock, paper or scissors?: ";
	while (std::cin >> choice)
	{
		if (i > 2 || i < 0)
			i = 0;
		if (choice == "Rock" || choice == "rock")
			choice_value = 1;
		else if (choice == "Paper" || choice == "paper")
			choice_value = 2;
		else if (choice == "Scissors" || choice == "scissors")
			choice_value = 3;
		else
		{
			std::cout << "Don't cheat!\n";
			std::cout << "Rock, paper or scissors?: ";
			continue;
		}
		computer_playstyle[i] = ((choice_value * i) % 3) + 1;
		switch (computer_playstyle[i])
		{
		case 1:
			std::cout << "computer: Rock!\n";
			break;
		case 2:
			std::cout << "computer: Paper!\n";
			break;
		case 3:
			std::cout << "computer: Scissors!\n";
			break;
		default:
			std::cout << "Error, computer didn't choose neither rock, paper scissors\n";
			break;
		}
		fight_resolve = choice_value - computer_playstyle[i];
		switch (fight_resolve)
		{
		case 0:
			std::cout << "draw.\n";
			break;
		case -1:
			std::cout << "computer wins.\n";
			break;
		case -2:
			std::cout << "You win.. this time!\n";
			break;
		case 1:
			std::cout << "You win.. this time!\n";
			break;
		case 2:
			std::cout << "computer wins.\n";
			break;
		default:
			std::cout << "Oops, something went wrong\n";
		}

		++i;
		std::cout << "Rock, paper or scissors?: ";
	}
	system("pause");
	return 0;
}