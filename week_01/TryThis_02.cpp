#include "std_lib_facilities.hpp"


int main()
{
	cout << "Please enter a integer value: ";
	int n;
	double m;
	cin >> n;
	cout << "n == " << n
		<< "\nn+1 == " << n + 1
		<< "\nthree times n == " << 3 * n
		<< "\ntwice n == " << n + n
		<< "\nn squared == " << n*n
		<< "\nhalf of n == " << n / 2;
		m = n;
		std::cout << "\nsquare root of n == " << sqrt(m)
		<< "\nmodulo of n from 10 == " << (n % 10)
		<< '\n'; // another name for newline (�end of line�) in output
	system("pause");
	return 0;
}