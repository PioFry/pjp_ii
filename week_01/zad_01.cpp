#include "std_lib_facilities.hpp"


void with_ints()
{
	int val1, val2;
	std::cout << "Enter two integer values: ";
	std::cin >> val1 >> val2;
	if (val1 > val2)
	{
		std::cout << "the bigger value is: " << val1 << "\n";
		std::cout << "the smaller value is: " << val2 << "\n";
	}
	if (val2 > val1)
	{
		std::cout << "the smaller value is: " << val1 << "\n";
		std::cout << "the bigger value is: " << val2 << "\n";
	}
	std::cout << "sum: " << (val1 + val2) << "\n";
	std::cout << "difference: " << (val1 - val2) << "\n";
	std::cout << "product: " << (val1 * val2) << "\n";
	std::cout << "ratio: " << (val1 / val2) << "\n";
}

void with_doubles()
{
	double val1, val2;
	std::cout << "Enter two floating point values: ";
	std::cin >> val1 >> val2;
	if (val1 > val2)
	{
		std::cout << "the bigger value is: " << val1 << "\n";
		std::cout << "the smaller value is: " << val2 << "\n";
	}
	if (val2 > val1)
	{
		std::cout << "the smaller value is: " << val1 << "\n";
		std::cout << "the bigger value is: " << val2 << "\n";
	}
	std::cout << "sum: " << (val1 + val2) << "\n";
	std::cout << "difference: " << (val1 - val2) << "\n";
	std::cout << "product: " << (val1 * val2) << "\n";
	std::cout << "ratio: " << (val1 / val2) << "\n";
}

int main()
{
	with_ints();
	// with doubles
	with_doubles();
	system("pause");
	return 0;
}